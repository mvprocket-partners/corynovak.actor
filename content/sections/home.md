---
video: https://www.youtube.com/embed/QlESFF_yC2A
videoTitle: Dramatic Reel - Cory Novak
soundCloudUrl: https://soundcloud.com/cory-novak-641813254/cory-novak-commercial-vo-demo
avatar: ../assets/cory-novak-headshots-18-of-18-.jpg
---
Connect with talent. Cory Joseph Novak is a talented:

* Actor
* Producer
* Director
* Writer
---
image: ../assets/cory-novak-headshots-3-of-5-.jpg
---
Novak studied Theatre and Film at the University of Missouri - Columbia (Mizzou), where he sharpened his acting and filmmaking skills. In addition to acting in several student film projects, Novak performed as one of the leads in the original and award-winning play Irish Rose at Mizzou's 24-hour play festival. After graduation, Novak was 1 of 16 people throughout the world who were accepted into and graduated from the Graduate-Level Professional Acting for the Camera Program at UCLA's School of Theater, Film & Television.
import Typography from "typography"
import moragaTheme from "typography-theme-moraga"

moragaTheme.headerWeight = 'lighter'
moragaTheme.baseFontSize = '20px' // default was 20px
moragaTheme.includeNormalize = true

moragaTheme.overrideThemeStyles = ({ rhythm }, options) => ({
  'h1,h2,h3': {
    marginBottom: rhythm(1/2),
    marginTop: rhythm(2),
  }
})
const typography = new Typography(moragaTheme)
 
// Output CSS as string.
typography.toString()
 
// Or insert styles directly into the <head> (works well for client-only
// JS web apps.
typography.injectStyles()

export default typography
export const rhythm = typography.rhythm
export const scale = typography.scale

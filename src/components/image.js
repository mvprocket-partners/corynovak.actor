import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import Img from "gatsby-image"

/*
 * This component is built using `gatsby-image` to automatically serve optimized
 * images with lazy loading and reduced file sizes. The image is loaded using a
 * `useStaticQuery`, which allows us to load the image from directly within this
 * component, rather than having to pass the image data down from pages.
 *
 * For more information, see the docs:
 * - `gatsby-image`: https://gatsby.dev/gatsby-image
 * - `useStaticQuery`: https://www.gatsbyjs.org/docs/use-static-query/
 */

const Image = ({imgName}) => {
  const data = useStaticQuery(graphql`
    query {
      headShotImage: file(relativePath: { eq: "mark-novak.jpg" }) {
        childImageSharp {
          fluid {
            ...GatsbyImageSharpFluid_withWebp_noBase64
          }
        }
      },
      familyImage: file(relativePath: { eq: "novak-family-photo.jpg" }) {
        childImageSharp {
          fluid {
            ...GatsbyImageSharpFluid_withWebp_noBase64
          }
        }
      },
      parentsImage: file(relativePath: { eq: "mark-and-lisa.jpg" }) {
        childImageSharp {
          fluid {
            ...GatsbyImageSharpFluid_withWebp_noBase64
          }
        }
      },
    }
  `);
  
  let imgData = data.parentsImage.childImageSharp.fluid;

  if (imgName === "family") {
    imgData = data.familyImage.childImageSharp.fluid;
  }
  else if (imgName === "headshot") {
    imgData = data.headShotImage.childImageSharp.fluid;
  }

  return (
    <Img fluid={imgData} />
  );
}

export default Image;

/**
 * SEO component that queries for data with
 *  Gatsby's useStaticQuery React hook
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from "react"
import PropTypes from "prop-types"
import Helmet from "react-helmet"
import { useStaticQuery, graphql } from "gatsby"

const SEO = ({ description, lang, meta, title }) => {
  const data = useStaticQuery(
    graphql`{
      markdownRemark(fields: {slug: {eq: "/settings/"}}) {
        frontmatter {
          siteTitle
          siteUrl
          description
          keywords
          twitter
          linkedin
          instagram
          facebook
          youtube
        }
      }
    }`
  )
  
  const metaDescription = description || data.markdownRemark.frontmatter.description;
  const siteTitle = title || data.markdownRemark.frontmatter.siteTitle;
  return (
    <Helmet
      htmlAttributes={{
        lang,
      }}
      title={siteTitle}
      meta={[
        {
          name: `description`,
          content: metaDescription,
        },
        {
          name: "keywords",
          content: data.markdownRemark.frontmatter.keywords.join(","),
        },
        {
          property: `og:title`,
          content: siteTitle,
        },
        {
          property: `og:description`,
          content: metaDescription,
        },
        {
          property: `og:type`,
          content: `website`,
        },
      ].concat(meta)}
    />
  )
}

SEO.defaultProps = {
  lang: `en`,
  meta: [],
  description: ``,
}

SEO.propTypes = {
  description: PropTypes.string,
  lang: PropTypes.string,
  meta: PropTypes.arrayOf(PropTypes.object),
  title: PropTypes.string.isRequired,
}

export default SEO

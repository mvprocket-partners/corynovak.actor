const path = require(`path`)
const { createFilePath } = require(`gatsby-source-filesystem`)

exports.onCreateNode = ({ node, actions, getNode }) => {
  const { createNodeField } = actions

  // Ensures we are processing only markdown files
  if (node.internal.type === `MarkdownRemark`) {
    if (node.fileAbsolutePath.match(/sections/i)) {
      // Use `createFilePath` to turn yaml files in our `content/sections` directory into `/slug`
      const relativeFilePath = createFilePath({
        node,
        getNode,
        basePath: "content/sections/",
        trailingSlash: true
      })
      // Creates new query'able field with name of 'slug'
      createNodeField({
        node,
        name: `slug`,
        value: `${relativeFilePath}`,
      })
    }
  }
}

exports.createSchemaCustomization = ({ actions }) => {
  const { createTypes } = actions
  // define these types so that the social frontmatter can be empty/null in settings/seo query
  const typeDefs = `
    type MarkdownRemarkFrontmatter implements Node {
      siteTitle: String
      siteUrl: String
      description: String
      imdb: String
      twitter: String
      linkedin: String
      instagram: String
      facebook: String
      youtube: String
    }
  `
  createTypes(typeDefs)
}